#!/usr/bin/env bash

echo "Configure script to exit when any command fails: set -e"
set -e


################################################################

cd  /var/www/clients/client0/web6/web

echo "Git pull from develop branch"
git pull origin develop

echo "Composer install"
composer install

echo "Migrate"
php artisan migrate

echo "generate l5-swagger"
php artisan l5-swagger:generate

echo "deployment seems to be successfull"
