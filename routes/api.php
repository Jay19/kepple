<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Avtar
Route::post('uploadAvatar','Api\AvatarController@uploadAvatar');
Route::post('getAvatarUrl','Api\AvatarController@getAvatarUrl');
Route::post('isUserValid','Api\AvatarController@isUserValid');
Route::post('deleteAvatar','Api\AvatarController@deleteAvatar');
///Session
Route::post('uploadSaveSession','Api\AvatarController@uploadSaveSession');
Route::post('GetAllSaveSessions','Api\AvatarController@GetAllSaveSessions');
Route::post('DeleteSaveSession','Api\AvatarController@deleteSaveSession');


//Voice
Route::post('UploadVoiceNote','Api\AvatarController@uploadVoiceNote');
Route::post('AddSessionReferenceToVoiceNote','Api\AvatarController@AddSessionReferenceToVoiceNote');
Route::post('RemoveSessionReferenceToVoiceNote','Api\AvatarController@RemoveSessionReferenceToVoiceNote');
