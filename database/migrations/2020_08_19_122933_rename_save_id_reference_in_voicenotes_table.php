<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameSaveIdReferenceInVoicenotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voicenotes', function (Blueprint $table) {
            $table->renameColumn('save_id_reference', 'save_id_references');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voicenotes', function (Blueprint $table) {
            $table->renameColumn('save_id_references', 'save_id_reference');
        });
    }
}
