<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginSessions extends Model
{
    protected $fillable =['login_session_id','user_id','is_online','login_time','last_activity'];
    protected $table = 'login_sessions';
}
