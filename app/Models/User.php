<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable =['cp_user_id','avatar_filepath','has_finished_uploading'];

    const IMAGE = 'image';
    const DATA = 'data';
    const VIDEO = 'video';
    const MISCELLANEOUS = 'miscellaneous';
    const _3D = '3d';
    const VOICE = 'voice';

    protected $table = 'users';


    public function getAvatarFilepathAttribute()
    {
        if (!$this->attributes['avatar_filepath']) {
            return null;
        }
        return \Storage::disk('obs')->url($this->attributes['avatar_filepath']);
    }
}
