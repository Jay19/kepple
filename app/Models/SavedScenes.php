<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SavedScenes extends Model
{
    protected $fillable =['user_id','json_filepath'];
    protected $table = 'savedscenes';

    public function getJsonFilepathAttribute()
    {
        if (!$this->attributes['json_filepath']) {
            return null;
        }
        return \Storage::disk('obs')->url($this->attributes['json_filepath']);
    }
}
