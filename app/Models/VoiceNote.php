<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoiceNote extends Model
{
    protected $fillable = ['user_id','voice_note_filepath','is_deleted','save_id_references'];

    protected $table = 'voicenotes';

    public function getVoiceNoteFilepathAttribute()
    {
    
        if (!$this->attributes['voice_note_filepath']){
            return null;
        }
        return \Storage::disk('obs')->url($this->attributes['voice_note_filepath']);
    }
}
