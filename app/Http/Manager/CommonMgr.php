<?php


namespace App\Manager;


use App\Models\User;

class CommonMgr
{
    public function getImageExtensions()
    {
        $array = array("ANI", "BMP", "CAL", "FAX", "GIF", "IMG", "JBG", "JPE", "JPEG", "JPG", "MAC", "PBM", "PCD", "PCX", "PCT", "PGM", "PGM", "PNG", "PPM", "PSD", "RAS", "TGA", "WMF");
        $final_array = array_map('strtolower', $array);
        return array_values($final_array);
    }

    public function getVideoExtensions()
    {
        $array = array("ASF", "AVI", "MP4", "MPG", "MOV", "RM", "SWF", "WMV", "VOB", "FLV", "SRT", "3GP", "3G2", "M4V", "MPEG", "AVS", "PDS", "VRO", "MKV", "SQZ", "RMVB", "SMIL", "DVR-MS", "PRPROJ", "DIVX", "BIK", "BIN", "MTS", "SWI", "SCM", "YUV", "VF", "TS", "VEG", "MOD", "IMOVIEPROJ", "XVID", "FBR", "BSF", "OGV", "F4V", "TRP", "STX", "RCPROJECT", "HDMOV", "MXF", "MOI", "DREAM", "WTV", "TIX", "PSH", "WLMP", "CPI", "DMSM", "M2P", "BDM", "DVDMEDIA", "DZM", "DZP", "CAMPROJ", "R3D", "AEPX", "WEBM", "EDL", "ALE", "AVP", "FCPROJECT", "ISMV", "ISM", "OTRKEY", "SNAGPROJ", "DASH", "NTP", "DV", "FLC", "FLI", "DIR", "DXR", "ASX", "IFO", "SVI", "VIEWLET", "WPL", "WMX", "WMD", "3GPP", "3GP2", "WVX", "NVC", "AJP", "DCE", "OGM", "M2V", "EYE", "IMOVIEPROJECT", "MSWMM", "MJP", "TIVO", "SBK", "VDO", "IZZ", "IZZY", "FCP", "AVB", "AEP", "AMV", "D2V", "WCP", "SFVIDCAP", "DAT", "DPG", "TDA3MT", "PRO", "CAMREC", "VP6", "VP7", "M21", "MP21", "PIV", "NSV", "QTZ", "MQV", "RUM", "D3V", "ZMV", "DVX", "SMK", "M1PG", "TOD", "IVR", "PMF", "VLAB", "M2TS", "M2T", "RV", "WMMP", "ZM1", "ZM2", "ZM3", "TP", "FLH", "MNV", "MSDVD", "AMX", "EVO", "AXM", "MP21", "M21", "MGV", "PXV", "MTV", "RDB", "HKM", "MVD", "FBZ", "ARF", "MJ2", "VGZ", "SWT", "REC", "VC1", "TPR", "SMI", "ROQ", "PREL", "QTL", "MPV", "WP3", "VCV", "NCOR", "TP0", "GTS", "DCK", "VFZ", "VDR", "PGI", "TVS", "BDMV", "CLPI", "MPLS", "OGX", "BMK", "DMSS", "VSP", "XFL", "3P2", "RMS", "MPL", "MVE", "MVP", "TPD", "JTS", "MPGINDEX", "DV-AVI", "RMP", "SEQ", "VPJ", "F4P", "DZT", "CMPROJ", "CMREC", "VCPF", "CMMTPL", "CMMP", "AETX", "AAF", "NUV", "GFP", "MVY", "CST", "STL", "SCC", "SCREENFLOW", "PPJ", "PRTL", "JTV", "DPA", "DVR", "TSP", "RMD", "RSX", "DMSD", "DMSD3D", "PHOTOSHOW", "XEJ", "XEL", "XESC", "ISMC", "XLMV", "MVP", "VEP", "MSE", "USM", "AMC", "DCR", "DCR", "MK3D", "BDT3", "IRCP", "SIV", "CINE", "ARCUT", "PLPROJ", "RCD", "RVL", "EZT", "SMI", "PAC", "890", "CIP", "JSS", "DMX", "SEDPRJ", "USF", "TTXT", "AEC", "THP", "CED", "AVV", "MANI", "MXV", "Y4M", "RVID", "WVE", "FTC", "BMC", "KDENLIVE", "AVCHD", "HDV", "LRV", "GCS", "SDV", "VBC", "TVSHOW", "TVLAYER", "FPDX", "TREC", "MPROJ", "GIFV", "EYETV", "FFM", "IMOVIEMOBILE", "EVO", "VTT", "XML", "SEC", "WXP", "WSVE", "WFSP", "CPVC", "SBZ", "RCUT", "GXF", "RCREC", "G2M", "NFV", "VR", "N3R", "WVM", "MOOV", "QT", "IVF", "VID", "RTS", "MOVIE", "MPE", "3MM", "BYU", "FLX", "MVC", "M1V", "VFW", "LSX", "M4U", "RP", "SPL", "WM", "QTCH", "SMV", "60D", "BOX", "GVP", "SFD", "DIF", "STR", "PLAYLIST", "W32", "DMB", "SCN", "AVS", "PAR", "MP2V", "PVA", "M2A", "MP4V", "MJPG", "MODD", "VEM", "DAV", "SBT", "META", "IVA", "DDAT", "MOFF", "VCR", "DNC", "BS4", "PVR", "SML", "AVD", "MYS", "DLX", "QTM", "3GPP2", "FBR", "DV4", "OSP", "264", "DMSM3D", "PSSD", "AET", "VSE", "VS4", "MPSUB", "BNP", "PNS", "LREC", "K3G", "H264", "WOT", "VFT", "VIDEO", "IRF", "BDT2", "CAMV", "MVEX", "MPL", "BVR", "QTINDEX", "AVC", "CX3", "FFD", "KTN", "XMV", "YOG", "JMV", "F4F", "MP4", "INFOVID", "AWLIVE", "PRO4DVD", "PRO5DVD", "PROQC", "INP", "INT", "TDT", "TID", "CLK", "BU", "SFERA", "LVIX", "VIX", "TVRECORDING", "ANX", "AXV", "VMLT", "VMLF", "THEATER", "IMOVIELIBRARY", "EXO", "FCARCH", "G64", "CMV", "MPG4", "WGI", "SAN", "FLIC", "F4M", "CREC", "AEGRAPHIC", "AECAP", "JDR", "RTS", "GVI", "GL", "GRASP", "LSF", "IVS", "M4E", "VIV", "VIVO", "MPV2", "BIX", "CVC", "MVB", "SSM", "MSH", "SCM", "DSY", "MPG2", "MPF", "MMV", "MPEG4", "VP3", "M15", "M75", "SEC", "RMD", "787", "TDX", "ANIM", "MOB", "PMV", "CEL", "AM", "VDX", "RMV", "GOM", "PJS", "AQT", "PSB", "SSF", "ZEG", "NUT", "MPEG1", "KMV", "ISMCLIP", "ORV", "DB2", "MPEG2", "MJPEG", "MT2S", "AVM", "V264", "FVT");
        $final_array = array_map('strtolower', $array);
        return array_values($final_array);
    }

    public function getModelExntensions()
    {
        $array = array("fbx", "glTF", "GLTF", "gltf", "glb");
        $final_array = array_map('strtolower', $array);
        return array_values($final_array);
    }

    public function getDataExtensions()
    {
        $array = array("doc", "odt", "pdf", "rtf", "tex", "txt", "wpd");
        $final_array = array_map('strtolower', $array);
        return array_values($final_array);
    }

    public function getVoiceExtensions()
    {
        $array = array("wav", "M4A", "AAC");
        $final_array = array_map('strtolower', $array);
        return array_values($final_array);

    }

    public function headerDetails()
    {
        $headers = getallheaders();
        $headers['ip_address'] = request()->ip();
        $headers = array_change_key_case($headers, CASE_UPPER);
        if (!isset($headers['USER-AGENT'])) {
            $headers['USER-AGENT'] = 'Unknown';
        }
        return $headers;
    }

    public static function generateToken($size = 10)
    {
        return md5(rand(1, $size) . microtime());
    }

    public function token()
    {
        //Generate a random string.
        $token = openssl_random_pseudo_bytes(64);
        //Convert the binary data into hexadecimal representation.
        return bin2hex($token);
    }

    //extensions
    public function getAllExtensions()
    {
        $img_arr_ext = $this->getImageExtensions();
        $video_arr_ext = $this->getVideoExtensions();
        $model_arr_ext = $this->getModelExntensions();
        $voice_arr_ext = $this->getVoiceExtensions();
        //merge into one array
        $final_array = array_merge($img_arr_ext, $video_arr_ext, $model_arr_ext,$voice_arr_ext);

        //all array value converted in lower case
        $final_array = array_map('strtolower', $final_array);

        $final_array = array_values($final_array);
        return $final_array;
    }


    //validate extensions
    public function validateExtension($index_name)
    {
        $allowed_extension = $this->getAllExtensions();
        // Get file file extension
        $file_extension = pathinfo($_FILES[$index_name]["name"], PATHINFO_EXTENSION);
        $file_extension = strtolower($file_extension);

        // Validate file input to check if is with valid extension
        if (in_array($file_extension, $allowed_extension)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getFileType($extention)
    {
        $img_arr_ext = $this->getImageExtensions();
        $video_arr_ext = $this->getVideoExtensions();
        $model_arr_ext = $this->getModelExntensions();
        $data_arr_ext = $this->getDataExtensions();
        $voice_arr_ext = $this->getVoiceExtensions();
        if (in_array($extention, $img_arr_ext)) {
            return User::IMAGE;
        } elseif (in_array($extention, $video_arr_ext)) {
            return User::VIDEO;
        } elseif (in_array($extention, $model_arr_ext)) {
            return User::_3D;
        } elseif (in_array($extention, $data_arr_ext)) {
            return User::DATA;
        }elseif(in_array($extention, $data_arr_ext)) {
            return User::VOICE;
        } else {
            return User::MISCELLANEOUS;
        }

    }
}
