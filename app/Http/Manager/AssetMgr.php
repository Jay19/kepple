<?php


namespace App\Manager;


use Illuminate\Support\Facades\Storage;

class AssetMgr
{

    protected $disk;
    protected $commonMgr;

    public function __construct(CommonMgr $commonMgr)
    {
        $this->disk = Storage::disk('obs');
        $this->commonMgr = $commonMgr;
    }


    /**
     * @desc this function return proper name after removing extra symbols form file name
     * @param $file
     * @return string
     */
    public function getProperName($file, $filename = null)
    {
        if ($filename == null) {
            $name = $file->getClientOriginalName();
        } else {
            $name = $filename;
        }

        $file_ext = $file->getClientOriginalExtension();
        //remove extension
        $getOnlyFileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $name);
        //remove extra symbols
        $repair = array(".", ",", " ", ";", "'", "\\", "\"", "/", "(", ")", "?");
        $name = str_replace($repair, "", $getOnlyFileName);
        return $name . '.' . $file_ext;
    }

    public function getFileInfo($file, $filename = null)
    {
        $result['size'] = $file->getSize();
        $result['file_name'] = $this->getProperName($file, $filename);
        $result['file_extension'] = $file->getClientOriginalExtension();
        $result['file_type'] = $this->commonMgr->getFileType($result['file_extension']);
         return $result;
    }

}
