<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Manager\CommonMgr;
use App\Manager\AssetMgr;
use App\Models\User;
use App\Models\SavedScenes;
use App\Models\VoiceNote;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Obs\ObsException;

class AvatarController extends Controller
{
    protected $request;
    protected $commonMgr;
    protected $assetMgr;
    protected $disk;

    public function __construct(Request $request, CommonMgr $commonMgr, AssetMgr $assetMgr)
    {
        $this->assetMgr = $assetMgr;
        $this->request = $request;
        $this->disk = Storage::disk('obs');
        $this->commonMgr = $commonMgr;
    }

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Keppel Digital Twin Api's",
     *      description="This is Api Documetation for Keppel Digital Twin Api's Description",
     *      @OA\Contact(
     *          email="jay.solanki@xcitech.in"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url="https://keppel.hiverlab.com/api/",
     *      description="API Server"
     * )
     *
     * @OA\Tag(
     *     name="Api List's",
     *
     * )
     */

    /**
     * @OA\Post(
     * path="uploadAvatar",
     * summary="Upload USer Avatar",
     * operationId="upload Avatar",
     *  tags={"Avatar"},
     * @OA\Parameter(
     * name="Avatar",
     * in="query",
     * required=true,
     * description="available file types :.glb ",
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Parameter(
     * name="UserId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function uploadAvatar()
    {
        $validator = Validator::make($this->request->all(), [
            'Avatar' => 'required',
            'UserId' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }
        $input = $this->request->all();
        DB::beginTransaction();
        try {
            $file = $this->request->file('Avatar');
            $data = $this->assetMgr->getFileInfo($file, "Avatar");
            $path = 'users/user-' . $input['UserId'] . '/avatar/' . $data['file_name'];
            if ($this->disk->has($path)) {
                $this->disk->delete($path);
            }
            try {
                $oosReference = $this->disk->write($path, $file);
            } catch (ObsException $e) {
                return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e);
            }
            if ($oosReference) {
                $index['cp_user_id'] = $input['UserId'];
                $index['avatar_filepath'] = $path;
                $index['has_finished_uploading'] = 0;

                $getAvatarUrl = User::where('cp_user_id', $input['UserId'])->count();
                if ($getAvatarUrl > 0) {
                    $AvatarUrl = User::where('cp_user_id', $input['UserId'])->where('avatar_filepath', $path)->update($index);

                    $response['code'] = 200;
                    $response['status'] = 'Success';
                    return response()->json($response);
                } else {
                    $index['cp_user_id'] = $input['UserId'];
                    $index['avatar_filepath'] = $path;
                    $index['has_finished_uploading'] = 0;

                    $avtar = new User($index);
                    $avtar->save();
                }
                DB::commit();
                $response['code'] = 200;
                $response['status'] = 'Success';
                return response()->json($response);
                //  return $this->successResponse(self::CODE_OK, "Voice Note Successfully Uploaded!!", $row);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e->getMessage());
        }
        return $this->errorResponse(self::CODE_UNAUTHORIZED, "User not found.");
    }

    /**
     * @OA\Post(
     * path="getAvatarUrl",
     * summary="get User Avtar",
     * operationId="getAvatarUrl",
     *  tags={"Avatar Url"},
     * @OA\Parameter(
     * name="UserId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function getAvatarUrl()
    {
        $input = $this->request->all();
        $getAvatarUrl = User::where('cp_user_id', $input['UserId'])->select('avatar_filepath')->first();

        if ($getAvatarUrl) {
            $response['code'] = 200;
            $response['status'] = "success";
            $response['avatar-url'] = $getAvatarUrl->avatar_filepath;

            return response()->json($response);
        } else {
            return $this->errorResponse(self::CODE_UNAUTHORIZED, "Session not found.");
        }
    }

    /**
     * @OA\Post(
     * path="deleteAvatar",
     * summary="delete Avatar",
     * operationId="delete Avatar",
     *  tags={"Avatar"},
     * @OA\Parameter(
     * name="UserId",
     * in="query",
     * required=true,
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function deleteAvatar()
    {
        $validator = Validator::make($this->request->all(), [
            'UserId' => 'required|exists:users,cp_user_id',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }
        $input = $this->request->all();
        $row = DB::table('users')->where('cp_user_id', $input['UserId'])->first();
        if ($row != null) {
            $deleted = $this->disk->has($row->avatar_filepath);
            if ($deleted) {
                $this->disk->delete($row->avatar_filepath); //voice note delete
                $data['avatar_filepath'] = "";
            }
            $update = User::where('cp_user_id', $input['UserId'])->update($data);
            return $this->successResponse(self::CODE_OK, self::OPERATION_SUCCESS);
        } else {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, ['UserId' => "UserId is Invalid"]);
        }
    }

    /**
     * @OA\Post(
     * path="isUserValid",
     * summary="is User Valid",
     * operationId="isUserValid",
     *  tags={"User Account Validation"},
     * @OA\Parameter(
     * name="UserId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function isUserValid()
    {
        $validator = Validator::make($this->request->all(), [
            'UserId' => 'required|exists:users,cp_user_id',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }
        $input = $this->request->all();
        $getUserId = User::where('cp_user_id', $input['UserId'])->get();
        if ($getUserId) {
            $response['code'] = 200;
            $response['status'] = "success";
            $response['avatar-url'] = $getUserId;

            return response()->json($response);
        } else {
            return $this->errorResponse(self::CODE_UNAUTHORIZED, "User Not Found !!");
        }
    }

    /**
     * @OA\Post(
     * path="uploadSaveSession",
     * summary="Upload Save Session",
     * operationId="uploadSaveSession",
     *  tags={"Upload Save Session"},
     * @OA\Parameter(
     * name="JsonFile",
     * in="query",
     * required=true,
     * description="available file types :.json ",
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Parameter(
     * name="UserId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function uploadSaveSession()
    {
        $validator = Validator::make($this->request->all(), [
            'JsonFile' => 'required',
            'UserId' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }

        //check file exists or not
        if (!$this->request->hasFile('JsonFile')) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, ['JsonFile' => ['JsonFile not exists!!']]);
        }
        $input = $this->request->all();

        DB::beginTransaction();
        try {
            $file = $this->request->file('JsonFile');
            $data = $this->assetMgr->getFileInfo($file);

            $path = 'users/user-' . $input['UserId'] . '/products/DigitalTwin/Keppel/sessions/' . $data['file_name'];
            if ($this->disk->has($path)) {
                $this->disk->delete($path);
            }
            try {
                $oosReference = $this->disk->write($path, $file);
            } catch (ObsException $e) {
                return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e);
            }
            if ($oosReference) {
                $index['user_id'] = $input['UserId'];
                $index['json_filepath'] = $path;
                $getsaveSession = SavedScenes::where('user_id', $input['UserId'])->where('json_filepath', $path)->count();

                if ($getsaveSession > 0) {
                    $session = SavedScenes::where('user_id', $input['UserId'])->where('json_filepath', $path)->update($index);

                } else {
                    $index['user_id'] = $input['UserId'];
                    $index['json_filepath'] = $path;

                    $session = new SavedScenes($index);
                    $session->save();
                }
                $row = SavedScenes::where('user_id', $input['UserId'])->where('json_filepath', $path)->first();
                DB::commit();
                return $this->successResponse(self::CODE_OK, "Json File Uploaded!!", $row);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }

    /**
     * @OA\Post(
     * path="DeleteSaveSession",
     * summary="deleteSaveSession",
     * operationId="deleteSaveSession",
     *  tags={"Session"},
     * @OA\Parameter(
     * name="SessionId",
     * in="query",
     * required=true,
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function deleteSaveSession()
    {
        $validator = Validator::make($this->request->all(), [
            'SessionId' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }
        $input = $this->request->all();
        $row = DB::table('savedscenes')->where('save_id', $input['SessionId'])->first();
        if ($row != null) {
            $this->disk->delete($row->json_filepath);
            DB::table('savedscenes')->where('save_id', $input['SessionId'])->delete();
            return $this->successResponse(self::CODE_OK, self::OPERATION_SUCCESS);
        }
        return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, ['SessionId' => "SessionId is Invalid"]);
    }

    /**
     * @OA\Post(
     * path="GetAllSaveSessions",
     * summary="Get All SaveSessions",
     * operationId="GetAllSaveSessions",
     *  tags={"GetAllSaveSessions"},
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function GetAllSaveSessions()
    {
        $getAllSaveSessions = SavedScenes::query();
        $getAllSaveSessions = $getAllSaveSessions->select('save_id', 'user_id as user-id', 'json_filepath')->get()->map(function ($data) {

            //  $data['user-id'] ='user-'. $data->user_id;
            $data->url = $data->json_filepath;

            unset($data->json_filepath);
            //  unset($data->user_id);
            return $data;
        });
        if ($getAllSaveSessions) {
            $response['code'] = 200;
            $response['status'] = "success";
            $response['data'] = $getAllSaveSessions;
            return response()->json($response);
        } else {
            return $this->errorResponse(self::CODE_UNAUTHORIZED, "Session not found.");
        }
    }

    /**
     * @OA\Post(
     * path="uploadVoiceNote",
     * summary="Upload Voice Note",
     * operationId="uploadVoiceNote",
     *  tags={"uploadVoiceNote"},
     * @OA\Parameter(
     * name="WavFile",
     * in="query",
     * required=true,
     * description="available file types :.wav",
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Parameter(
     * name="SaveId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Parameter(
     * name="UserId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function uploadVoiceNote()
    {
        $validator = Validator::make($this->request->all(), [
            'UserId' => 'required',
            'WavFile' => 'required',
            'SaveId' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }
        //check file exists or not
        $input = $this->request->all();
        DB::beginTransaction();
        try {
            $file = $this->request->file('WavFile');
            $data = $this->assetMgr->getFileInfo($file);
            $path = 'users/user-' . $input['UserId'] . '/assets/audio/' . $data['file_name'];

            if ($this->disk->has($path)) {
                $this->disk->delete($path);
            }
            try {
                $oosReference = $this->disk->write($path, $file);
            } catch (ObsException $e) {
                return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e);
            }
            if ($oosReference) {
                $index['user_id'] = $input['UserId'];
                $index['voice_note_filepath'] = $path;
                $index['save_id_references'] = $input['SaveId'];

                $getVoiceNote = VoiceNote::where('user_id', $input['UserId'])->where('voice_note_filepath', $path)->count();

                if ($getVoiceNote > 0) {
                    VoiceNote::where('user_id', $input['UserId'])->where('voice_note_filepath', $path)->update($index);
                } else {
                    $index['user_id'] = $input['UserId'];
                    $index['voice_note_filepath'] = $path;
                    $index['save_id_references'] = $input['SaveId'];

                    $response = new VoiceNote($index);
                    $response->save();
                }
                $response = VoiceNote::where('user_id', $input['UserId'])->where('voice_note_filepath', $path)->first();
                DB::commit();
                return $this->successResponse(self::CODE_OK, "Voice Note Successfully Uploaded!!", $response);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e->getMessage());
        }
        return $this->errorResponse(self::CODE_UNAUTHORIZED, "Session not found.");

    }

    /**
     * @OA\Post(
     * path="AddSessionReferenceToVoiceNote",
     * summary="Add Session Reference To VoiceNote",
     * operationId="AddSessionReferenceToVoiceNote",
     *  tags={"AddSessionReferenceToVoiceNote"},
     * @OA\Parameter(
     * name="VoiceNoteId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Parameter(
     * name="SessionId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function AddSessionReferenceToVoiceNote()
    {
        $validator = Validator::make($this->request->all(), [
            'VoiceNoteId' => 'required|exists:voicenotes,voice_note_id',
            'SessionId' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }
        $input = $this->request->all();

        try {
            $voicenotes = VoiceNote::where('voice_note_id', $input['VoiceNoteId'])->first();
            // if ($voicenotes) {
            $ref = $voicenotes->save_id_references;
            $ref = explode(',', $ref);
            $ref[] = $input['SessionId'];
            $ref = array_unique($ref);
            $ref = implode(',', $ref);

            $data['save_id_references'] = $voicenotes->save_id_references = $ref;
            $update = VoiceNote::where('voice_note_id', $voicenotes->voice_note_id)->update($data);

            return $this->successResponse(self::CODE_OK, "Successfully Add Session!!", $voicenotes);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e->getMessage());
        }

    }

    /**
     * @OA\Post(
     * path="RemoveSessionReferenceToVoiceNote",
     * summary="Remove Session Reference To VoiceNote",
     * operationId="RemoveSessionReferenceToVoiceNote",
     *  tags={"RemoveSessionReferenceToVoiceNote"},
     * @OA\Parameter(
     * name="VoiceNoteId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Parameter(
     * name="SessionId",
     * in="query",
     * required=true,
     * description="Success",
     * @OA\Schema(
     * type="number"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Success",
     * @OA\MediaType(
     * mediaType="application/json",
     * )
     * ),
     * @OA\Response(
     * response=401,
     * description="Unauthorized"
     * ),
     * @OA\Response(
     * response=400,
     * description="Invalid request"
     * ),
     * @OA\Response(
     * response=404,
     * description="not found"
     * ),
     * )
     */
    public function RemoveSessionReferenceToVoiceNote()
    {
        $validator = Validator::make($this->request->all(), [
            'VoiceNoteId' => 'required|exists:voicenotes,voice_note_id',
            'SessionId' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorResponse(self::CODE_INVALID_REQUEST, self::INVALID_REQUEST, $validator->errors());
        }
        $input = $this->request->all();
        $deleteVoice = VoiceNote::where('voice_note_id', $input['VoiceNoteId'])->first();
        DB::beginTransaction();
        try {
            $ref = $deleteVoice->save_id_references;
            $ref = explode(',', $ref);

            for ($i = 0; $i < count($ref); $i++) { //count session Id
                if ($ref[$i] == $input['SessionId']) {
                    unset($ref[$i]); // unset session
                }
            }
            if (count($ref) == 0) {
                $voice = DB::table('voicenotes')->where('voice_note_id', $input['VoiceNoteId'])->first();
                $exists = $this->disk->has($voice->voice_note_filepath);
                if ($exists) {
                    $this->disk->delete($voice->voice_note_filepath); //voice note delete
                }
                $data['save_id_references'] = "";
            } else {
                $ref = array_unique($ref);
                $ref = implode(',', $ref);
                $data['save_id_references'] = $ref;
            }
            $update = VoiceNote::where('voice_note_id', $deleteVoice->voice_note_id)->update($data);
            DB::commit();
            return $this->successResponse(self::CODE_OK, "Successfully Deleted!!", $update);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse(self::CODE_INTERNAL_SERVER_ERROR, self::INTERNAL_SERVER_ERROR, $e->getMessage());
        }
    }
}
